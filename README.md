<img src="./screenshot/screenshot.jpg" style="max-width:200px"/>

<br>
Godot developers can use the [xrfragment.gd](https://codeberg.org/coderofsalvation/xrfragment-godot/src/branch/main/xrfragment.gd) library to build their own XR browser.<br><br>
There's an [Example Godot Project](https://codeberg.org/coderofsalvation/xrfragment-godot) included which uses it using this simple [main.gd](https://codeberg.org/coderofsalvation/xrfragment-godot/src/branch/main/main.gd) script.
<br><br>
<b>NOTE:</b> the XR Fragment support is not as mature as the AFRAME library (see Example Model Browser in sidemenu)

* demo video: https://coderofsalvation.github.io/xrfragment.media/xrfragment-godot.mp4
